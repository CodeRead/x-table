﻿using DotLiquid;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

public class LiquidDemo 
{

    public class Student
    {
        public int Age { get; set; }

        public string m_Name;

        public string Name
        {
            get { return m_Name; }
        }
    }

    [MenuItem("My/Test")]
    private static void MenuItem_Test()
    {
        var templateContent = File.ReadAllText("Assets/TestTemplate.txt");
        var lqTemplate = Template.Parse(templateContent);

        var templateHash = new Hash();

        // todo 逻辑部分
        templateHash["int"] = 123;
        templateHash["float"] = 10.56f;
        templateHash["str"] = "Hello World";
        templateHash["StrArray"] = new string[] { "1", "2" };
        var intList = new List<int>();
        intList.Add(1);
        intList.Add(2);
        intList.Add(3);
        templateHash["intList"] = intList;

        var dict = new Dictionary<string, object>();
        dict.Add("a", 1);
        dict.Add("b", 2);
        dict.Add("c", 3);
        templateHash["dict"] = dict;

        var dict2 = new Dictionary<int, string>();
        dict2.Add(1, "a");
        dict2.Add(2, "b");
        dict2.Add(3, "c");
        templateHash["dict2"] = dict2;

        var stu = new Student();
        stu.Age = 20;
        stu.m_Name = "ZhangSan";
        templateHash["obj"] = Hash.FromAnonymousObject(stu); //不用Hash包装下, 会报错

        var dictObj = new Dictionary<string, object>();
        dictObj.Add("Age", 20);
        dictObj.Add("Name", "ZhangSan");
        dictObj.Add("m_Name", "ZhangSan");
        templateHash["dictObj"] = dictObj;

        var stuList = new List<Hash>();
        for (int i = 0; i < 3; ++i)
        {
            var stu2 = new Student();
            stu2.m_Name = $"Stu_{i}";
            stu2.Age = 20 + i;
            stuList.Add(Hash.FromAnonymousObject(stu2));
        }
        templateHash["stuList"] = stuList;

        using (var sw = new StreamWriter("Assets/temp.txt", false, Encoding.UTF8))
        {
            var rdParams = new RenderParameters();
            rdParams.LocalVariables = templateHash;
            rdParams.RethrowErrors = true;
            lqTemplate.Render(sw, rdParams);
        }

        AssetDatabase.Refresh();
        Debug.Log("finish!!!");
    }

}
